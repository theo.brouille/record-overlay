import SwiftUI
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    var overlayViewPresenter: OverlayViewPresenter?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = scene as? UIWindowScene else {
            return
        }
        
        let window = UIWindow(windowScene: windowScene)
        
        window.rootViewController = UIHostingController(rootView: RootView())
        window.makeKeyAndVisible()
    
        overlayViewPresenter = OverlayViewPresenter(window: window)
        
        self.window = window
    }
}
