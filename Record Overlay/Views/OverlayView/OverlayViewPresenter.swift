import UIKit
import SwiftUI

final class OverlayViewPresenter {
    private let window: UIWindow
    private var overlayVC: UIHostingController<OverlayView>
    
    init(window: UIWindow) {
        self.window = window
        self.overlayVC = UIHostingController(rootView: OverlayView())
        
        self.addOverlay()
    }
    
    func addOverlay() {
        overlayVC.view.translatesAutoresizingMaskIntoConstraints = false
        overlayVC.view.sizeToFit()
        overlayVC.view.backgroundColor = .clear
        
//        overlayVC.view.isUserInteractionEnabled = false
        
        if let parentVC = window.rootViewController {
            overlayVC.didMove(toParent: parentVC)
            overlayVC.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(moveOverlay)))
        }
        
        overlayVC.view.center.y = 500
        
        self.window.addSubview(overlayVC.view)
    }
    
    @objc private func moveOverlay(_ sender: UIPanGestureRecognizer) {
        overlayVC.view.center = sender.location(in: window.rootViewController?.view)
    }
}
