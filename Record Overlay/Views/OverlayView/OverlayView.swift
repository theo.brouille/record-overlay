import SwiftUI

struct OverlayView: View {
    var body: some View {
        Text(UIDevice.current.identifierForVendor?.uuidString ?? "")
            .fixedSize()
            .padding(10)
            .background(Color.blue)
            .cornerRadius(4)
            .opacity(0.7)
    }
}

struct OverlayView_Previews: PreviewProvider {
    static var previews: some View {
        OverlayView()
    }
}
