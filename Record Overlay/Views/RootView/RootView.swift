import SwiftUI

struct RootView: View {
    @State private var isScreenCaptured: Bool = UIScreen.main.isCaptured
    
    var body: some View {
//        ZStack {
            VStack {
                Image(systemName: isScreenCaptured ? "stop.circle" : "video.circle")
                    .resizable()
                    .frame(width: 100, height: 100)
                    .foregroundColor(isScreenCaptured ? .red : Color(UIColor.label))
                
                Text(isScreenCaptured ? "Recording..." : "Not recording")
                
                Button("Test") {
                    isScreenCaptured.toggle()
                }
            }
            .onReceive(NotificationCenter.default.publisher(for: UIScreen.capturedDidChangeNotification)) { _ in
                self.isScreenCaptured = UIScreen.main.isCaptured
            }
            
//            OverlayView()
//        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
